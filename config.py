roleChannelID = 600855006303879178
MITGuildID = 523718048612614164
# Dictionaries per message that hold information of what role applies to what reaction
firstHalfRoleEmojiDict = {
    "🌉": 599464847305998338,  # Course 1 Civil
    "⚙": 599465359870918656,  # Course 2 Mechanical
    "💎": 599470572782616576,  # Course 3 Material
    "🏛": 599470900928053249,  # Course 4 Architecture
    "⚗": 599470953101262848,  # Course 5 Chemistry
    "🖥": 599471001587417088,  # Course 6 EECS
    "🌿": 599471071632293889,  # Course 7 Biology
    "⚛": 599471121779130400,  # Course 8 Physics
    "💭": 599471205141053442,  # Course 9 Brain and Cognitive Studies
    "💥": 599471258366902282,  # Course 10 Chemical Engineering
    "🏙": 599471545718669327,  # Course 11 Planning/Urban Planning
    "🌎": 599471613871783937,  # Course 12 Earth and Planetary Sciences
}

secondHalfRoleEmjoiDict = {
    "💵": 599471668175568936,  # Course 14 Economics
    "🏬": 599474286306000907,  # Course 15 Management/Business
    "🚀": 599475070636916778,  # Course 16 Aerospace
    "🗣": 599475118011580436,  # Course 17 Political Science
    "🔢": 599475282771968022,  # Course 18 Mathematics
    "☣": 599475344147480577,  # Course 20 Biological Engineering
    "🎭": 599475413541978132,  # Course 21 Culture/Media
    "☢": 599475467455561749,  # Course 22 Nuclear Science
    "🤔": 599475657512321024,  # Course 24 Philosophy/Linguistics
    "🎞": 599475700189364245,  # CMS Comparative Media Studies
    "🔬": 599475749484888067,  # STS Science, Technology, and Society
}

pronounRoleEmojiDict = {
    "❤": 600860011618369546,  # She/Her Pronoun
    "💜": 600860203822481408,  # They/Them Pronoun
    "💙": 600860353814986754,  # He/Him Pronoun
}


messageIDEmojiDict = {
    600857640658141186: firstHalfRoleEmojiDict,
    600858512427581471: secondHalfRoleEmjoiDict,
    600859769665880074: pronounRoleEmojiDict,
}
